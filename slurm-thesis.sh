#!/bin/bash
#SBATCH --account=cseduproject
#SBATCH --partition=csedu
#SBATCH --qos=csedu-large                   
#SBATCH --cpus-per-task=5                   
#SBATCH --mem=20G                           
#SBATCH --gres=gpu:5                 
#SBATCH --time=01:00:00            
#SBATCH --output=./slurm_logs/%j-%x.out
#SBATCH --error=./slurm_logs/%j-%x.err
#SBATCH --mail-user=coen.schoof@ru.nl
#SBATCH --mail-type=END,FAIL
#SBATCH --job-name=thesis
#SBATCH -w cn48
#SBATCH --ntasks-per-node=1

#VENV_PATH="/ceph/csedu-scratch/project/cschoof/thesis/venv_thesis/bin/activate"
VENV_PATH="/scratch/cschoof/venv_thesis/bin/activate"
#MAIN_PATH=/ceph/csedu-scratch/project/cschoof/thesis/emotional-trigger/train.py
#YAML_PATH=/ceph/csedu-scratch/project/cschoof/thesis/emotional-trigger/train.yaml
MAIN_PATH=/ceph/csedu-scratch/project/cschoof/thesis/emotional-trigger/train_speaker_embeddings.py
YAML_PATH=/ceph/csedu-scratch/project/cschoof/thesis/emotional-trigger/hyperparams.yaml

source $VENV_PATH

LISTNODES=`scontrol show hostname $SLURM_JOB_NODELIST`
MASTER=`echo $LISTNODES | cut -d" " -f1`

torchrun --nproc_per_node=5 --nnodes=1 --node_rank=${SLURM_NODEID} --node_rank=${SLURM_NODEID} --master_addr=${MASTER}  $MAIN_PATH $YAML_PATH #--master_port=5555

# CREMA_PATH="/scratch/cschoof/thesis/emotional-trigger/datasets/CREMA"
# EMO-DB_PATH="/scratch/cschoof/thesis/emotional-trigger/datasets/EMO-DB/wav"
# ESD-EN_PATH="/scratch/cschoof/thesis/emotional-trigger/datasets/ESD/Emotion Speech Dataset/ESD-en"
# ESD-ZH_PATH="/scratch/cschoof/thesis/emotional-trigger/datasets/ESD/Emotion Speech Dataset/ESD-en"
# RAVDESS_PATH="/scratch/cschoof/thesis/emotional-trigger/datasets/RAVDESS"


#python /ceph/csedu-scratch/project/cschoof/thesis/emotional-trigger/main.py
#--device='cpu' #crema $CREMA_PATH WSI ANG largecnn

# deactivate



